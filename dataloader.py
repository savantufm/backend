import firebase_admin
from firebase_admin import (
    credentials,
    firestore,
    db
)
from twython import Twython  
import json
import re

def get_text(text):
    print(text)
    regex = r'https?://[^\s<>"]+|www\.[^\s<>"]+'
    match = re.search(regex, text)
    if re.findall(r"#(\w+)", text) or re.findall(r'@+[a-zA-Z0-9]', text):
        print('ok...')
    elif match:
        return text
            
def extract_link(text):
    regex = r'https?://[^\s<>"]+|www\.[^\s<>"]+'
    match = re.search(regex, text)
    if match:
        return match.group()
    return ''

def getTweets(doc):
            
    with open("twitter_credentials.json", "r") as file:  
        creds = json.load(file)

    # Instantiate an object
    python_tweets = Twython(creds['CONSUMER_KEY'], creds['CONSUMER_SECRET'])

    # Create our query
    query = {'q': doc['nombre'],  
            'result_type': 'popular',
            'count': 15,
            'lang': 'en',
            'include_entities': False,
            'tweet_mode': 'extended',
            }

    # Search tweets
    dict_ = []
    for status in python_tweets.search(**query)['statuses']:
        tweet = {
            'user': status['user']['screen_name'],
            'date': status['created_at'],
            'link': extract_link(status['full_text']),
            'text': status['full_text'],
            'favorite_count': status['favorite_count']
        }       
        dict_.append(tweet)
    
    return dict_

cred = credentials.Certificate('savant_ufm.json')
firebase_admin.initialize_app(cred, {
    "databaseURL": "https://savant-ufm.firebaseio.com/",
})
store = firestore.client()

topics_ref = store.collection(u'topics')
docs = topics_ref.get()


for doc in docs:
    print(u'{} => {}'.format(doc.id, doc.to_dict()))
    tweets = getTweets(doc.to_dict())
    ref = db.reference('topics')
    ref.child(doc.to_dict()['nombre']).delete()
    for tweet in tweets:
        ref.child(doc.to_dict()['nombre']).push().set(tweet)
    